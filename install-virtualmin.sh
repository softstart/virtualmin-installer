#!/bin/bash

readlink /proc/$$/exe | grep -q bash
RET=$?
if [ $RET != 0 ]; then
  echo
  echo "This script is intended to run using bash, you can either:"
  echo "chmod u+x install-virtualmin.sh && ./install-virtualmin.sh"
  echo "or /bin/bash install-virtualmin.sh"
  echo
  exit 1
fi


if [ ! -f ./slib.sh ]; then
  echo
  echo "Downloading slib ..."
  echo

  # Download the slib (source: http://github.com/virtualmin/slib)
  # Lots of little utility functions.
  curl -s -S -L https://software.virtualmin.com/lib/slib.sh -o slib.sh
  if [ ! -f ./slib.sh ]; then
    echo
    echo "Library slib.sh could not be downloaded, cancelling the install... "
    echo
    exit 3
  fi
fi

. ./slib.sh

#
# Parse args
#
shopt -s extglob
ALLOWED_PHP_VERSIONS="5.6,7.1,7.2,7.4,8.0"
WITH_ELASTICSEARCH=0
NO_ELASTICSEARCH_SWAP=0
WITH_PHP=("7.4")

POSITIONAL=()
while [[ $# -gt 0 ]]; do
  key="$1"

  case $key in
    --with-elasticsearch)
      WITH_ELASTICSEARCH=1
      shift
      ;;
    --with-php*)
      if [[ "$1" =~ ^--with-php(${ALLOWED_PHP_VERSIONS//,/|})$ ]];
      then
        WITH_PHP+=(${BASH_REMATCH[1]})
      else
        log_error "Unrecognized PHP version! Allowed versions are: ${ALLOWED_PHP_VERSIONS[@]}"
        exit 1
      fi
      shift
      ;;
    --no-elasticsearch-swap)
      NO_ELASTICSEARCH_SWAP=1
      shift
      ;;
    *)
      POSITIONAL+=("$1") # save it in an array for later
      shift
      ;;
  esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

elastic_memory_ok(){
  mem_total=$(awk '/MemTotal/ {print $2}' /proc/meminfo)
  swap_total=$(awk '/SwapTotal/ {print $2}' /proc/meminfo)
  all_mem=$((mem_total + swap_total))

  min_mem=4194304
  min_mem_h=$((min_mem / 1024))
  if [ $all_mem -lt $min_mem ]; then
      #Memory available is less than 4GB, 4GB swap will be created
      log_error "Memory available is below ${min_mem_h} MB."
      log_info  "A ${min_mem_h} MB swap file will be created."
    
      # Check for btrfs, because it can't host a swap file safely.
      root_fs_type=$(grep -v "^$\\|^\\s*#" /etc/fstab | awk '{print $2 " " $3}' | grep "/ " | cut -d' ' -f2)
      if [ "$root_fs_type" = "btrfs" ]; then
          log_error "Your root filesystem appears to be running btrfs. It is unsafe to create"
          log_error "a swap file on a btrfs filesystem. You'll either need to use the --no-elasticsearch-swap"
          log_error "installation or create a swap file manually (on some other filesystem)."
          exit 1
      fi

      # Check for enough space.
      root_fs_avail=$(df /|grep -v Filesystem|awk '{print $4}')
      if [ "$root_fs_avail" -lt $((min_mem + 358400)) ]; then
          root_fs_avail_h=$((root_fs_avail / 1024))
          log_error "Root filesystem only has $root_fs_avail_h MB available, which is too small."
          log_error "You'll either need to use the --no-elasticsearch-swap installation or add more space to '/'."
          exit 1
      fi

      # Create a new file
      log_info "Creating a new swap file"
      if ! dd if=/dev/zero of=/swapfile bs=1024 count=$min_mem; then
          log_error "Creating swap file /swapfile failed."
          exit 1
      fi
      chmod 0600 /swapfile
      mkswap /swapfile
      if ! swapon /swapfile ; then
          log_error "Enabling swap file failed. If this is a VM, it may be prohibited by your provider."
          exit 1
      fi
      if ! grep  '\/swapfile\s*swap\s*swap\s*defaults\s*0\s*0'  /etc/fstab; then
        echo "/swapfile          swap            swap    defaults        0 0" >> /etc/fstab
      fi
  fi
}

if [ "${WITH_ELASTICSEARCH}" = "1" ] && [ "${NO_ELASTICSEARCH_SWAP}" = "0" ]; then
  elastic_memory_ok
fi

#
# Join elements of an array
#
implode () {
  local IFS="$1"
  shift
  echo "$*"
}

# Sort PHP versions
WITH_PHP=($(implode '
' "${WITH_PHP[@]}" | sort -u))

HOST=$1

if [ -z "$HOST" ]; then
  HOST=$(hostname)
  if [ "$HOST" == "${HOST/./}" ]; then
    echo
    echo "It is expected for the server to have a fully qualified domain name (FQDN)."
    echo "Please fix that first or if you are an advanced user, you can pass the hostname"
    echo "to use as a parameter to the script"
    echo "More info: https://www.virtualmin.com/documentation/installation/automated#fqdn"
    echo
    echo "On Ubuntu 20.04 LTS, you can use a command as below. Please remember"
    echo "to replace server1.example.org by your real (sub)domain name"
    echo
    echo "sudo hostnamectl set-hostname server1.example.org"
    echo
    echo "Then, restart your server which the command below (after which you try again the installation)"
    echo
    echo "reboot"
    echo
    exit 1
  fi
else
  if [ "$HOST" == "${HOST/./}" ]; then
    echo
    echo "The host passed as a parameter is not a FQDN, please review that"
    echo
    exit 1
  fi
fi

HOSTCMD=host
if [ ! command -v host ] &>/dev/null && [ command -v nslookup ] &>/dev/null; then
  HOSTCMD=nslookup
fi

if [ ! $HOSTCMD $HOST ] &>/dev/null; then
  if [ "_${IGNORE_HOST}" != "_y" ]; then
    echo
    echo "Not able to resolve hostname '$HOST', refusing to continue"
    echo "If you want to force that, set the env IGNORE_HOST=y when calling the script"
    echo "eg. IGNORE_HOST=y $*"
    echo
    exit 2
  fi
  echo
  echo "Forcing the script to ignore that the host name is not resolvable"
  echo
fi

# Make sure cwd is the same folder as the script.
cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1


if [ -x "/usr/sbin/resolvconf" ];
then
  /usr/sbin/resolvconf --disable-updates
elif [ -f "/etc/NetworkManager/NetworkManager.conf" ];
then
  sed -i -e 's/^\[main\]/[main]\ndns=none/' /etc/NetworkManager/NetworkManager.conf
  systemctl restart NetworkManager
fi

# Discover and sets up distro version globals os_type, os_version, os_major_version, os_real
get_distro

if [ "$os_type" = "debian" ];
then
    update-alternatives --set iptables /usr/sbin/iptables-legacy
    update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
fi

#
# Install Virtualmin
#

echo
echo "Installing Virtualmin using the Hostname '$HOST' ..."
echo

if [ -f /usr/sbin/virtualmin ]; then
  echo
  echo "* Virtualmin already installed, skipping ..."
  echo
else
  echo
  echo "* Downloading Virtualmin ..."
  echo

  curl -s -S -L http://software.virtualmin.com/gpl/scripts/install.sh -o install.sh

  echo
  echo "* Installing LAMP stack with minimal setup ..."
  echo

  /bin/sh install.sh --hostname $HOST --minimal --bundle LAMP --force --yes

  rm install.sh
fi

#
# Run WikiSuite Installer
#

echo
echo
echo "* Installing WikiSuite..."
echo

LOGFILE=wikisuite-installer.log

# slib config
LOG_LEVEL_STDOUT="${LOG_STDOUT:-INFO}"
LOG_LEVEL_LOG="${LOG_LOG:-DEBUG}"
LOG_PATH="$LOGFILE"
RUN_LOG="$LOGFILE"

# slib fixes
if [ "$(declare -Ff "log_fatal")" != "log_fatal" ] ; then
  log_fatal() { log_error "$1"; }
fi

# similar to slib setconfig, that is not working
configvalue() {
  sc_config="$2"
  sc_value="$1"
  sc_directive=$(echo "$sc_value" | cut -d'=' -f1 | sed 's/^[ \t]*//;s/[ \t]*$//')
  if grep -q "$sc_directive" "$2"; then
    sed -i -e "s#$sc_directive.*#$sc_value#" "$sc_config"
  else
    echo "$1" >>"$2"
  fi
}

# Helper functions (extensions to slib)
run() {
  RUN_ERRORS_FATAL=1
  log_debug "Running command: $1"
  run_ok "$1" "$2"
  RUN_ERRORS_FATAL=
}

run_fail_ok() {
  log_debug "Running: $1"
  run_ok "$1" "$2"
}

run_file_exists() {
  if [ ! -f "$3" ] || [ ! -d "$3" ] ; then
    return 0
  fi
  run "$1" "$2"
}

run_file_missing() {
  if [ -f "$3" ] || [ -d "$3" ] ; then
    return 0
  fi
  run "$1" "$2"
}

run_fail_ok_file_missing() {
  if [ -f "$3" ]; then
    return 0
  fi
  run_fail_ok "$1" "$2"
}


WEBMIN_ETC="/etc/webmin"
# set variables based on OS type
case "$os_type" in
"fedora" | "centos" | "rhel" | "amazon")
  WEBMIN_PATH="/usr/libexec/webmin"
  WEBMIN_CLI="/usr/libexec/webmin/bin/webmin"
  ;;
"debian" | "ubuntu")
  export DEBIAN_FRONTEND=noninteractive
  WEBMIN_PATH="/usr/share/webmin"
  WEBMIN_CLI="/usr/share/webmin/bin/webmin"
  ;;
esac

log_info "Started WikiSuite installation log in $LOGFILE"

echo
log_debug "Downloading Dependencies"
printf "${YELLOW}▣${CYAN}□${NORMAL}: Downloading Dependencies\\n"

ASSETS_DIR=/tmp/virtalmin-install-$$
ASSETS_PACKAGE_URL=${ASSETS_PACKAGE_URL:-"https://gitlab.com/wikisuite/virtualmin-installer/-/archive/master/virtualmin-installer-master.tar.gz"}

download_assets () {
  mkdir ${ASSETS_DIR}
  curl --silent --fail -o ${ASSETS_DIR}/installer-assets.tgz "${ASSETS_PACKAGE_URL}"  >/dev/null 2>&1 || {
      echo
      echo "Asset ${ASSET} could not be downloaded, cancelling the install... "
      echo
      exit 3
    }
  tar -C "${ASSETS_DIR}" -zxf "${ASSETS_DIR}/installer-assets.tgz" --strip-components=2 virtualmin-installer-master/assets/
  rm -f ${ASSETS_DIR}/installer-assets.tgz
}

run "download_assets" "Download Assets"

echo
log_debug "Updating Operating System"
printf "${YELLOW}▣${CYAN}□${NORMAL}: Updating Operating System\\n"

case "$os_type" in
"fedora" | "centos" | "rhel" | "amazon")
  run_fail_ok "yum install -y yum-fastestmirror" "Install Faster mirror plugin for Yum"
  run_fail_ok "yum install -y deltarpm yum-presto" "Install Presto plugin for Yum"
  run "yum -y update" "Updating the packages installed"
  ;;
"debian" | "ubuntu")
  run "apt-get update" "Updating the package repository information"
  run "apt-get upgrade -y" "Updating the packages installed"
  ;;
esac

echo
log_debug "Installing Multiple PHP Versions"
printf "${YELLOW}▣${CYAN}□${NORMAL}: Installing Multiple PHP Versions\\n"

case "$os_type" in
"fedora" | "centos" | "rhel" | "amazon")
  run_file_missing "yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-${os_major_version}.noarch.rpm" "Enabling EPEL repositories" /etc/yum.repos.d/epel.repo
  run_file_missing "yum install -y http://rpms.remirepo.net/enterprise/remi-release-${os_major_version}.rpm" "Enabling Remi repositories (Multiple PHP Versions)" /etc/yum.repos.d/remi.repo
  run_file_missing "yum install -y https://download1.rpmfusion.org/free/el/rpmfusion-free-release-${os_major_version}.noarch.rpm" "Enabling RPM Fusion repository" /etc/yum.repos.d/remi.repo
  ;;
"debian")
  run "apt-get -y install lsb-release apt-transport-https ca-certificates" "Installing dependencies to enable apt-repositories"
  run "curl -s -S -L -o /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg" "Download Multiple PHP repo gpg key"
  run "echo 'deb https://packages.sury.org/php/ $(lsb_release -sc) main' > /etc/apt/sources.list.d/php-packages.sury.org.list" "Configure Multiple PHP repo source definition"
  run "apt-get update" "Update repository info"
  ;;
"ubuntu")
  run "apt-get install -y software-properties-common" "Installing dependencies to enable apt-repositories"
  run "add-apt-repository -y ppa:ondrej/php" "Enabling repos for multiple PHP versions"
  run "apt-get update" "Update repository info"
  ;;
esac

# All supported versions (21.x LTS, 18.x LTS, etc.) of Tiki run on either PHP 5.6, 7.1, 7.2 or 7.4 (PHP 7.0 and 7.3 are not useful)

deb_configure_php_versions() {
  (
    set -x
    for PHP_VERSION in ${WITH_PHP[@]}; do
      {
        echo "; local configurations for php engines not cli"
        echo "; priority=99"
        echo "memory_limit = 160M"
      } >"/etc/php/${PHP_VERSION}/mods-available/local-config-not-cli.ini"

      {
        echo "; local configurations for php engines not cli"
        echo "; priority=99"
        echo "date.timezone = UTC"
        echo "upload_max_filesize = 20M"
        echo "post_max_size = 20M"
        echo "disable_functions = system"
      } >"/etc/php/${PHP_VERSION}/mods-available/local-config.ini"

      phpenmod -v ${PHP_VERSION} -s ALL local-config || return 1

      for SAPI in apache2 cgi fpm; do
        if [ -d /etc/php/${PHP_VERSION}/${SAPI} ]; then
          phpenmod -v ${PHP_VERSION} -s ${SAPI} local-config-not-cli || return 1
        fi
      done

      systemctl restart php${PHP_VERSION}-fpm || return 1
    done
  )
}

rpm_configure_php_versions() {
  (
    set -x
    for PHP_VERSION in 56; do
      config_file="/opt/remi/php${PHP_VERSION}/root/etc/php.ini"
      if [ -f "${config_file}" ]; then
        sed -i -e '/memory_limit\s*=/ s/= .*/= 160M/' "${config_file}"
        sed -i -e '/upload_max_filesize\s*=/ s/= .*/= 20M/' "${config_file}"
        sed -i -e '/post_max_size\s*=/ s/= .*/= 20M/' "${config_file}"
        sed -i -e '/disable_functions\s*=/ s/= .*/= system/' "${config_file}"
        sed -i 's/;date.timezone =/date.timezone = UTC/' "${config_file}"
      fi
    done

    for PHP_VERSION in 71 72 74 80; do
      config_file="/etc/opt/remi/php${PHP_VERSION}/php.ini"
      if [ -f "${config_file}" ]; then
        sed -i -e '/memory_limit\s*=/ s/= .*/= 160M/' "${config_file}"
        sed -i -e '/upload_max_filesize\s*=/ s/= .*/= 20M/' "${config_file}"
        sed -i -e '/post_max_size\s*=/ s/= .*/= 20M/' "${config_file}"
        sed -i -e '/disable_functions\s*=/ s/= .*/= system/' "${config_file}"
        sed -i 's/;date.timezone =/date.timezone = UTC/' "${config_file}"
      fi
    done
  )
}

PACKAGES=""
PHP_VERSIONS_STR="${WITH_PHP[@]}"
case "$os_type" in
"fedora" | "centos" | "rhel" | "amazon")
  for PHP_VERSION in ${WITH_PHP[@]//./}; do
    VERSION_PACKAGES="php${PHP_VERSION} php${PHP_VERSION}-php-cli php${PHP_VERSION}-php-mysql php${PHP_VERSION}-php-xml php${PHP_VERSION}-php-zip php${PHP_VERSION}-php-bz2 php${PHP_VERSION}-php-curl php${PHP_VERSION}-php-gd php${PHP_VERSION}-php-mbstring php${PHP_VERSION}-php-intl php${PHP_VERSION}-php-ldap php${PHP_VERSION}-php-bcmath php${PHP_VERSION}-php-fpm php${PHP_VERSION}-php-soap php${PHP_VERSION}-php-sqlite3 php${PHP_VERSION}-php-pecl-imagick"
    PACKAGES="${PACKAGES} ${VERSION_PACKAGES}"
  done
  run "yum install -y ${PACKAGES}" "Installing multiple PHP versions (${PHP_VERSIONS_STR})"
  run "rpm_configure_php_versions" "Configuring multiple PHP versions (will restart the services)"
  ;;
"debian" | "ubuntu")
  # common list
  for PHP_VERSION in ${WITH_PHP[@]}; do
    VERSION_PACKAGES="php${PHP_VERSION}-cli php${PHP_VERSION}-cgi php${PHP_VERSION}-mysql php${PHP_VERSION}-xml php${PHP_VERSION}-zip php${PHP_VERSION}-bz2 php${PHP_VERSION}-curl php${PHP_VERSION}-gd php${PHP_VERSION}-mbstring php${PHP_VERSION}-intl php${PHP_VERSION}-ldap php${PHP_VERSION}-bcmath php${PHP_VERSION}-fpm php${PHP_VERSION}-soap php${PHP_VERSION}-common php${PHP_VERSION}-sqlite3 php${PHP_VERSION}-imagick"
    PACKAGES="${PACKAGES} ${VERSION_PACKAGES}"
  done

  for PHP_VERSION in 5.6 7.1; do
    if [[ "${WITH_PHP[@]}" =~ ${PHP_VERSION} ]];
    then
      VERSION_PACKAGES="php${PHP_VERSION}-mcrypt php${PHP_VERSION}-json"
      PACKAGES="${PACKAGES} ${VERSION_PACKAGES}"
    fi
  done

  for PHP_VERSION in 7.2 7.4; do
    if [[ "${WITH_PHP[@]}" =~ ${PHP_VERSION} ]];
    then
      VERSION_PACKAGES="php${PHP_VERSION}-json"
      PACKAGES="${PACKAGES} ${VERSION_PACKAGES}"
    fi
  done

  for PHP_VERSION in 8.0; do
    if [[ "${WITH_PHP[@]}" =~ ${PHP_VERSION} ]];
    then
      VERSION_PACKAGES=""
      PACKAGES="${PACKAGES} ${VERSION_PACKAGES}"
    fi
  done

  run "apt-get install -y ${PACKAGES}" "Installing multiple PHP versions (${PHP_VERSIONS_STR})"
  run "deb_configure_php_versions" "Configuring multiple PHP versions (will restart the services)"
  ;;
esac

#
# Setup default PHP version
#

case "$os_type" in
"fedora" | "centos" | "rhel" | "amazon")
  DEFAULT_PHP=74
  if [ "$(declare -Ff "module")" != "module" ] ; then
    . /etc/profile.d/modules.sh
  fi
  run "module load php${DEFAULT_PHP}"
  ;;
"debian" | "ubuntu")
  DEFAULT_PHP=7.4
  run "update-alternatives --set php /usr/bin/php${DEFAULT_PHP}" "Set PHP ${DEFAULT_PHP} as default php"
  run "update-alternatives --set phar /usr/bin/phar${DEFAULT_PHP}" "Set PHP ${DEFAULT_PHP} as default phar"
  run "update-alternatives --set phar.phar /usr/bin/phar.phar${DEFAULT_PHP}" "Set PHP ${DEFAULT_PHP} as default phar.phar"
  run_file_exists "update-alternatives --set php-fpm.sock /run/php/php${DEFAULT_PHP}-fpm.sock" "Set PHP ${DEFAULT_PHP} as default php-fpm.sock" /etc/alternatives/php-fpm.sock
  ;;
esac

#
# Installing Elasticsearch: https://doc.tiki.org/Elasticsearch
#

if [ "${WITH_ELASTICSEARCH}" = "1" ];
then
  case "$os_type" in
  "fedora" | "centos" | "rhel" | "amazon")
    run "yum install -y java-1.8.0-openjdk-devel" "Installing Java"
    {
      echo "[elasticsearch-6.x]"
      echo "name=Elasticsearch repository for 6.x packages"
      echo "baseurl=https://artifacts.elastic.co/packages/6.x/yum"
      echo "gpgcheck=1"
      echo "gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch"
      echo "enabled=1"
      echo "autorefresh=1"
      echo "type=rpm-md"
    } >"/etc/yum.repos.d/elasticsearch.repo"
    run "yum install -y elasticsearch" "Installing Elasticsearch"
    ;;

  "debian" | "ubuntu")
    curl -s -S -L https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
    echo "deb https://artifacts.elastic.co/packages/oss-6.x/apt stable main" >/etc/apt/sources.list.d/elastic-6.x.list
    run "apt-get update" "Updating apt repositories"
    run "apt-get install -y default-jre default-jdk" "Installing Java"
    # 2 steps because elastic need to find JAVA_HOME
    run "apt-get install -y elasticsearch-oss" "Installing Elasticsearch"
    ;;
  esac

  #Update elasticsearch Xms and Xmx
  #Elasticsearch: You should always set the min and max JVM heap size to the same value.
  if ! sed -i "s/-Xms[0-9]g\+/-Xms4g/" /etc/elasticsearch/jvm.options; then
      log_error "Set Xms option in /etc/elasticsearch/jvm.options failed."
  fi

  if ! sed -i "s/-Xmx[0-9]g\+/-Xmx4g/" /etc/elasticsearch/jvm.options; then
      log_error "Set Xmx option in /etc/elasticsearch/jvm.options failed."
  fi

  run "systemctl enable elasticsearch.service" "Enabling Elasticsearch"
  run "systemctl restart elasticsearch.service" "Restarting Elasticsearch"
fi

PACKAGES_OTHER=() # initialize array

# Packages needed to install Tiki, use Composer, sysadmin tasks, etc
PACKAGES_OTHER+=("awstats" "bzip2" "etckeeper" "git" "htop" "iotop" "mc" "ncdu")
PACKAGES_OTHER+=("patch" "rkhunter" "rsync" "screen" "subversion" "tar" "tmux")
PACKAGES_OTHER+=("traceroute" "whois" "zip")

# Packages related to text Extraction / File Gallery / Unified Search: https://doc.tiki.org/Search-within-files
PACKAGES_OTHER+=("catdoc")

# Packages related to Media-Alchemyst: https://doc.tiki.org/Media-Alchemyst
PACKAGES_OTHER+=("libreoffice" "ffmpeg" "unoconv" "ghostscript" "gpac")

# Packages related to synchronization
PACKAGES_OTHER+=("awscli")

# Packages related to File Gallery Search Indexing: https://doc.tiki.org/Search-within-files
PACKAGES_OTHER+=("elinks")

# Packages related to Tiki Manager: https://doc.tiki.org/Manager
PACKAGES_OTHER+=("sqlite")

case "$os_type" in
"fedora" | "centos" | "rhel" | "amazon")
  PACKAGES_OTHER+=("ImageMagick" "perl-DBD-SQLite" "perl-Digest-HMAC" "perl-Email-Valid" "perl-File-Fetch" "perl-JSON" "perl-XML-Simple" "perl-YAML-Tiny" "pstotext" "tesseract")
  ;;
"debian" | "ubuntu")
  PACKAGES_OTHER+=("imagemagick" "libdbd-sqlite3-perl" "libdigest-hmac-perl"  "libemail-valid-perl" "libjson-perl" "libxml-simple-perl" "libyaml-tiny-perl" "syncthing" "tesseract-ocr-all" "tesseract-ocr")
  ;;
esac

# Packages related to other Tiki Functions
PACKAGES_OTHER+=()

case "$os_type" in
"fedora" | "centos" | "rhel" | "amazon")
  run "yum install -y ${PACKAGES_OTHER[*]}" "Installing other packages needed / expected..."
  ;;
"debian" | "ubuntu")
  run "apt-get install -y ${PACKAGES_OTHER[*]}" "Installing other packages needed / expected..."
  ;;
esac

fix_imagick() {
  (
    set -x
    # Enabling Imagick to read and write PDF files
    # Ubuntu, Debian
    if [ -f /etc/ImageMagick-6/policy.xml ]; then
      sed -i -e 's/<policy domain="coder" rights="none" pattern="PDF" \/>/<policy domain="coder" rights="read|write" pattern="PDF" \/>/' /etc/ImageMagick-6/policy.xml
    fi
    # centos
    if [ -f /etc/ImageMagick6/ImageMagick-6/policy.xml ]; then
      sed -i -e 's/<policy domain="coder" rights="read|write" pattern="{GIF,JPEG,PNG,WEBP}" \/>/<policy domain="coder" rights="read|write" pattern="{GIF,JPEG,PNG,WEBP,PDF}" \/>/' /etc/ImageMagick6/ImageMagick-6/policy.xml
    fi
  )
}
run "fix_imagick" "Enable ImageMagick to read/write pdfs"

fix_rkhunter_logrotate() {
  cat > /etc/logrotate.d/rkhunter <<'EOF'
/var/log/rkhunter.log {
  weekly
  missingok
  rotate 4
  compress
  delaycompress
  notifempty
  create 640 root adm
}
EOF
}
run "fix_rkhunter_logrotate" "Fixes rkhunter logrotate"

run "curl -s -S -L 'https://gitlab.com/wikisuite/tiki-manager-for-virtualmin/-/raw/master/tikimanager.pl' -o ${WEBMIN_PATH}/virtual-server/scripts/tikimanager.pl" "Installing Tiki Manager script for Virtualmin"
run "curl -s -S -L 'https://gitlab.com/wikisuite/syncthing-for-virtualmin/-/raw/master/syncthing.pl' -o ${WEBMIN_PATH}/virtual-server/scripts/syncthing.pl" "Installing Syncthing script for Virtualmin"

echo
echo "* Refreshing Virtualmin configuration"
echo

log_debug "Configuring Virtualmin"
printf "${GREEN}▣${YELLOW}▣${NORMAL}: Configuring Virtualmin\\n"

#
# Adjust features we want enabled / disabled in Virtualmin
#

run_file_exists "virtualmin set-global-feature --disable-feature virtualmin-dav" "Disabling Virtualmin DAV" "${WEBMIN_PATH}/virtualmin-dav"
#run "virtualmin set-global-feature --disable-feature dns" "Disabling DNS"
#run "virtualmin set-global-feature --disable-feature mail" "Disabling Mail"
run "virtualmin set-global-feature --default-on ssl" "Enabling SSL"

#
# Setup Templates
#

virtualmin_modify_template_web () {
  (
    # works around escaping $ in the template when using "run"
    set -x
    APACHE_TEMPLATE=$(cat ${ASSETS_DIR}/apache-template | sed 's/\t/    /g' | tr '\n' '\t')
    virtualmin modify-template --id 0 --setting web --value "${APACHE_TEMPLATE}"
    return $?
  )
}

run "virtualmin_modify_template_web" "Modifying template Web"
run "virtualmin modify-template --id 0 --setting web_webmail --value 0" "Modifying template WebMail"
run "virtualmin modify-template --id 0 --setting web_admin --value 0" "Modifying template WebAdmin"
run "virtualmin modify-template --id 0 --setting web_stats_hdir --value stats" "Modifying template WebStats"
run "virtualmin modify-template --id 0 --setting web_phpver --value 7.4" "Modifying template Web PHP Version"
run "virtualmin modify-template --id 0 --setting web_php_suexec --value 3" "Modifying template PHP su exec"
run "virtualmin modify-template --id 0 --setting php_vars --value ''" "Modifying template PHP vars"
run "virtualmin modify-template --id 0 --setting php_fpm --value 'php_admin_value[sys_temp_dir] = $HOME/tmp'" "Modifying template PHP FPM pool"

echo
echo "* Setup Default Plan"
echo

run "virtualmin modify-plan --id 0 --no-quota" "Modifying plan quota"
run "virtualmin modify-plan --id 0 --no-admin-quota" "Modifying plan admin-quota"

#
# Sensible defaults
#
sed  -n  -e  '/^blockhost_failures=/!p'  -i -e  '$ablockhost_failures=30'  /etc/webmin/miniserv.conf
sed  -n  -e  '/^blockhost_time=/!p'      -i -e  '$ablockhost_time=60'      /etc/webmin/miniserv.conf
sed  -n  -e  '/^blockuser_failures=/!p'  -i -e  '$ablockuser_failures=35'  /etc/webmin/miniserv.conf
sed  -n  -e  '/^blockuser_time=/!p'      -i -e  '$ablockuser_time=60'      /etc/webmin/miniserv.conf
sed  -n  -e  '/^logouttime=/!p'          -i -e  '$alogouttime=60'          /etc/webmin/miniserv.conf

#
# Add plans and templates for WikiSuite
#
cp ${ASSETS_DIR}/plans/10                   ${WEBMIN_ETC}/virtual-server/plans
cp ${ASSETS_DIR}/plans/20                   ${WEBMIN_ETC}/virtual-server/plans
cp ${ASSETS_DIR}/plans/30                   ${WEBMIN_ETC}/virtual-server/plans
cp ${ASSETS_DIR}/templates/10               ${WEBMIN_ETC}/virtual-server/templates
cp ${ASSETS_DIR}/templates/20               ${WEBMIN_ETC}/virtual-server/templates
cp ${ASSETS_DIR}/templates/30               ${WEBMIN_ETC}/virtual-server/templates

# Set the default plan and template to be used
sed -n  -e  '/^init_plan=/!p'      -i -e  '$ainit_plan=10'    ${WEBMIN_ETC}/virtual-server/config
sed -n  -e  '/^init_template=/!p'  -i -e  '$ainit_template=10' ${WEBMIN_ETC}/virtual-server/config

#
# Setup default page template
#
mkdir -p ${WEBMIN_ETC}/authentic-theme
rsync -a ${ASSETS_DIR}/authentic-theme/ ${WEBMIN_ETC}/authentic-theme/

cp ${WEBMIN_PATH}/virtual-server/default/index.html ${WEBMIN_PATH}/virtual-server/default/index_orig.html
run "cat ${ASSETS_DIR}/index.html > ${WEBMIN_PATH}/virtual-server/default/index.html" "Deploying new default page template"

#
# Execute the wizard steps and setup the default vhost for the server
#

cp ${ASSETS_DIR}/wikisuite-auto-wizard.pl ${WEBMIN_PATH}/virtual-server/wikisuite-auto-wizard.pl
chmod a+x ${WEBMIN_PATH}/virtual-server/wikisuite-auto-wizard.pl

run "WIKISUITE_DEFHOST=$HOST virtualmin wikisuite-auto-wizard" "Auto executing configuration wizard"

rm ${WEBMIN_PATH}/virtual-server/wikisuite-auto-wizard.pl

#
# Refreshing Virtualmin configuration
#

run "virtualmin check-config" "Check Virtualmin configuration"

echo
echo "* Update MySQL Server configurations"
echo

case "$os_type" in
"fedora" | "centos" | "rhel" | "amazon")
  PATH_MYSQL_CNF=/etc/my.cnf
  MYSQL_SERVICE=mariadb
  ;;
"debian")
  PATH_MYSQL_CNF=/etc/mysql/mariadb.conf.d/50-server.cnf
  MYSQL_SERVICE=mysql
  ;;
"ubuntu")
  PATH_MYSQL_CNF=/etc/mysql/mysql.conf.d/mysqld.cnf
  MYSQL_SERVICE=mysql
  ;;
esac

if [ -f $PATH_MYSQL_CNF ]; then
  run "configvalue max_allowed_packet=16M ${PATH_MYSQL_CNF}" "MySQL: Updating max_allowed_packet"
  run "systemctl restart ${MYSQL_SERVICE}" "Restarting MySQL"
else
  log_warning "Unable to update max_allowed_packet, file $PATH_MYSQL_CNF does not exist."
fi

echo
echo "* Setup automatic weekly updates"
echo

set_weekly_updates() {
  (
    set -x
    # Automatic updates to be ran every friday at midnight
    (
      crontab -l | grep -v /package-updates/update.pl
      echo
      echo "0 0 * * 5 ${WEBMIN_PATH}/package-updates/update.pl"
    ) | crontab -
    ${WEBMIN_CLI} set-config -m package-updates -o sched_action -v 2 -f
  )
  return 0
}

run "set_weekly_updates" "Configure Webmin to update weekly"

install_tikimanager() {
  tikimanager_root=/opt/tiki-manager/app
  mkdir -p $(dirname "${tikimanager_root}")

  # clone Tiki Manager
  if [ ! -e "${tikimanager_root}/tiki-manager.php" ];
  then
    git clone https://gitlab.com/tikiwiki/tiki-manager.git "${tikimanager_root}"
  fi

  # create ssh key
  if [ ! -e "${tikimanager_root}/data/id_rsa" ];
  then
    ssh-keygen -f /opt/tiki-manager/app/data/id_rsa -N '' -C "tikimanager@${HOST}"
  fi

  php /opt/tiki-manager/app/tiki-manager.php
}
run "install_tikimanager" "Installing Tiki Manager"

install_virtualmin_tikimanager() {
  sed -i -e '/^root/s/$/ virtualmin-tikimanager/' "${WEBMIN_ETC}/webmin.acl"
  mkdir -p "${WEBMIN_ETC}/virtualmin-tikimanager"
  cat > "${WEBMIN_ETC}/virtualmin-tikimanager/config" <<'EOF'
tikimanager_home=/opt/tiki-manager/app
tikimanager=/opt/tiki-manager/app/tiki-manager
EOF
  git clone https://gitlab.com/wikisuite/virtualmin-tikimanager.git "${WEBMIN_PATH}/virtualmin-tikimanager"
}
run "install_virtualmin_tikimanager" "Installing Virtualmin Tiki Manager plugin"
run "virtualmin set-global-feature --enable-feature virtualmin-tikimanager" "Enabling Virtualmin Tiki Manager plugin"

block_unsafe_ports () {
  firewall-cmd --remove-port=20/tcp --permanent
  firewall-cmd --remove-port=21/tcp --permanent
  firewall-cmd --remove-port=1025-65535/tcp --permanent
  firewall-cmd --reload
}
run "block_unsafe_ports" "Blocking unsafe ports"

#
# Cleaning up
#
echo
printf "${GREEN}▣▣${NORMAL} Cleaning up\\n"

run "rm -fr ${ASSETS_DIR}" "Removing assets dir"
